# Refs^0.0.6 ^[wiki](https://gitlab.com/ancient/refs/wikis/home)

Implements interface references between different storages.

## Theory

###### `object` adapters

It is a object with user adapted methods of any of databases for some class.

A detailed description of the requirements to the adapter can be found in the [wiki](https://gitlab.com/ancient/refs/wikis/home).

[Adapters repository](https://gitlab.com/ancient/adapters).

###### `string` pattern Ref

It is a format of string for cross-adapters references between any data.

```js
"adapterName/uniqueId"
```

###### `Document` support

Returns data wrapped in a [Document](https://gitlab.com/ancient/document) capsule.

Adds to `FactoryDocumentConstructor` method `Ref`.

```js
document.Ref(); // "adapterName/uniqueId"
```

###### `class` Refs

![Chart](https://gitlab.com/ancient/refs/raw/master/chart.png)

## Example

```js
// Dependencies

var Adapters = require('ancient-adapters').Adapters;
var Refs = require('ancient-refs');
var DocumentsFactory = require('ancient-document').AdaptersFactory;
var documentsFactory = new DocumentsFactory(adapters);

// Demo storages

var Posts = {
  'post': { _id: 'post' }
};

var Users = {
  'iov': { name: 'iov' }
};

// Adapters for demo storages

var adapters = new Adapters();
adapters.add('posts', {

  // (id: String, callback: (error?, result?: any))
  getRef: function(id, callback) {
    callback(undefined, Posts[id]);
  },
  
  // (data: any) => id: String
  newRef: function(data) {
    return data._id;
  },
  
  // (id: String, callback: (error?, result?: Boolean))
  deleteRef: function(id, callback) {
    if (Posts[id]) {
      delete Posts[id];
      callback(undefined, true);
    } else {
      callback(undefined, undefined);
    }
  }
});
adapters.add('users', {

  // (id: String, callback: (error?, result?: any))
  getRef: function(id, callback) {
    callback(undefined, Users[id]);
  },
  
  // (data: any) => id: String
  newRef: function(data) {
    return data.name;
  },
  
  // (id: String, callback: (error?, result?: Boolean))
  deleteRef: function(id, callback) {
    if (Users[id]) {
      delete Users[id];
      callback(undefined, true);
    } else {
      callback(undefined, undefined);
    }
  }
});

// Refs operations

var refs = new Refs(documentsFactory);
refs.get('users/iov'); // Document { data: { name: 'iov' } }
refs.get('users/iov', function(error, data) {
  data; // Document { data: { name: 'iov' } }
});
refs.new('users', { name: 'iov' }); // 'users/iov';
refs.get('posts/post'); // Document { data: { _id: 'post' } }
refs.get('posts/post', function(error, data) {
  data; // Document { data: { _id: 'post' } }
});
refs.new('posts', { _id: 'post' }); // 'posts/post';
refs.delete('posts/post'); // true
refs.get('posts/post'); // undefined;

// Get refs from adapters

adapters.refs.get('posts/post'); // Document { data: { _id: 'post' } }
```

## Versions

### 0.0.6
* Just access to refs from adapters class.

### 0.0.5
* Support for new Adapters class.

### 0.0.4
* Document #6

### 0.0.3
* Method `parse(ref: Ref) => { name: String, adapter: Adapter, id: String }`.

### 0.0.2
* Method `delete` by ref.

### 0.0.1
* Short recording synchronous adapters.
* Wiki support.

### 0.0.0
* Sync and async getters.
* Generator of refs.