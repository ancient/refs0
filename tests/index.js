var assert = require('chai').assert;
var Refs = require('../index.js');
var test = require('./test.js');
var DocumentsFactory = require('ancient-document').Factory;
var Adapters = require('ancient-adapters').Adapters;

describe('ancient-refs', function() {
  var storage = {
    123: { _id: 123 },
    'abc': { _id: 'abc' }
  };
  
  var adapters = new Adapters();
  adapters.add('test', {
    
    // (id: String, callback: (error?, result?: any))
    getRef: function(id, callback) {
      if (callback) callback(undefined, storage[id]);
    },
    
    // (data: any) => id: String
    newRef: function(data) {
      return data._id;
    },
    
    // (id: String, callback: (error?, result?: Boolean))
    deleteRef: function(id, callback) {
      if (storage[id]) {
        delete storage[id];
        if (callback) callback(undefined, true);
      } else {
        if (callback) callback(undefined, undefined);
      }
    }
    
  });
  
  var documentsFactory = new DocumentsFactory(adapters);
  
  describe('test demo storage adapter', function() {
    test(Refs, documentsFactory, 'test', {
      'test/123': { _id: 123 },
      'test/abc': { _id: 'abc' }
    }, true, true);
  });
});