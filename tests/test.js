var assert = require('chai').assert;

module.exports = function(Refs, documentsFactory, adapterName, requiredResults, async, sync) {
  var refs;
  it('refs', function() {
    refs = new Refs(documentsFactory);
  });
  
  if (async) {
    for (var r in requiredResults) {
      (function(r) {
        it('getRef '+r+' async', function(done) {
          refs.get(r, function(error, document) {
            assert.deepEqual(document.data, requiredResults[r]);
            done(); 
          });
        });
      })(r);
    }
  }
  
  if (sync) {
    for (var r in requiredResults) {
      (function(r) {
        it('getRef '+r+' sync', function(done) {
          assert.deepEqual(refs.get(r).data, requiredResults[r]);
          done(); 
        });
      })(r);
    }
  }
  
  for (var r in requiredResults) {
    (function(r) {
      it('newRef '+r, function(done) {
        assert.deepEqual(refs.new(adapterName, requiredResults[r]), r);
        done(); 
      });
    })(r);
  }
  
  for (var r in requiredResults) {
    (function(r) {
      it('document.Ref '+r, function() {
        var document = refs.documentsFactory.new(adapterName, requiredResults[r]);
        assert.equal(document.Ref(), r);
      });
    })(r);
  }
  
  for (var r in requiredResults) {
    (function(r) {
      it('deleteRef '+r+' '+(sync?'sync/':'')+(async?'async':''), function(done) {
         var syncResult = refs.delete(r, function(error, asyncResult) {
           if (async) {
             assert.equal(asyncResult, true);
           }
         });
         if (sync) {
           assert.equal(syncResult, true);
         }
         done(); 
      });
    })(r);
  }
};