/** Adapters requirements **/

// getRef(id: String, callback: (error?, data?: any))
// newRef(data: any) => id: String
// deleteRef(id: String, callback: (error?, result?: Boolean))

/** Refs manager **/
// new (documentsFactory: DocumentsFactory)
var Refs = function(documentsFactory) {
  var refs = this;
  
  // document.Ref() => Ref
  this.documentsFactory = documentsFactory;
  documentsFactory.documentPrototype.Ref = function() {
    return refs.new(this.adapterName, this.data);
  };
  
  this.adapters = documentsFactory.adapters;
  
  this.adapters.refs = refs;
  
  for (var a in this.adapters.adapters) {
    this.adapterValidate(this.adapters.adapters[a].adapterName);
  }
};

// (adapterName: String) => undefined|thrown Error
Refs.prototype.adapterValidate = function(adapterName) {
  var adapter = this.adapters.get(adapterName);
  if (!adapter.getRef) throw new Error('Adapter "'+adapterName+'" is incompatible with Refs. Method "getRef" is not defined.');
  if (!adapter.newRef) throw new Error('Adapter "'+adapterName+'" is incompatible with Refs. Method "newRef" is not defined.');
  if (!adapter.deleteRef) throw new Error('Adapter "'+adapterName+'" is incompatible with Refs. Method "deleteRef" is not defined.');
};

// (ref: String, callback?: (error, document?: Document)) => document?: Document
Refs.prototype.get = function(ref, callback) {
  var refs = this;
  var ref = this.parse(ref);
  var result;
  ref.adapter.getRef(ref.id, function(error, _result) {
    result = refs.documentsFactory.new(ref.name, _result);
    if (callback) callback(error, result);
  });
  return result;
};

// (adapterName: String, data: any) => ref: Ref
Refs.prototype.new = function(adapterName, data) {
  var adapter = this.adapters.get(adapterName);
  if (!adapter) {
    throw new Error('Adapter "'+adapterName+'" is not defined.');
  }
  return adapterName+'/'+adapter.newRef(data);
};

// (ref: String, callback?: (error, result?: Boolean)) => result?: Boolean
Refs.prototype.delete = function(ref, callback) {
  var ref = this.parse(ref);
  var result;
  ref.adapter.deleteRef(ref.id, function(error, _result) {
    result = _result;
    if (callback) callback(error, result);
  });
  return result;
};

// (ref: String) => { name: String, adapter: Adapter, id: String }
Refs.prototype.parse = function(ref) {
  if (typeof(ref) != 'string')
    throw new Error('Invalid ref "'+ref+'"');
  var splited = ref.split('/');
  if (splited.length !== 2) 
    throw new Error('Invalid ref "'+ref+'"');
  var adapter = this.adapters.get(splited[0]);
  if (!adapter)
    throw new Error('Adapter "'+splited[0]+'" is not defined');
  return { name: splited[0], adapter: adapter, id: splited[1] };
};

module.exports = Refs;